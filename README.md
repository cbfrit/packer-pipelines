# Packer Pipelines
This repository contains GitLab CI pipelines for building server templates/images using [Packer](https://www.packer.io/).

## Usage
Currently only the VMware Fusion and VMware ESXi builders are working. To build on ESXi you need to create a `secrets/esxi.json` file with the following structure (populate the values):

```json
{
  "esxi_host": "",
  "esxi_username": "",
  "esxi_password": ""
}
```

To build on all available platforms, run
```shell
packer -var-file secrets/esxi.json centos.json
```

To build with only one platform, use the builder `name`, such as
```shell
packer -var-file secrets/esxi.json --only=esxi centos.json
```

To build on multiple (but not all) platforms, use a comma-separated list of builder `name`s, such as
```shell
packer -var-file secrets/esxi.json --only=esxi,fusion centos.json
```

## Exporting a VMware Fusion VM to ESXi
Use VMware's `ovftool`:
```shell
ovftool --compress=9 output-fusion/packer-fusion.vmx vi://user@esxi-server
```
